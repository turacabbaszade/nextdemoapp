import "../styles/login.css"
import "../styles/relatedItem.css"
import "../styles/mainpage.css"

import 'bootstrap/dist/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

export default function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}