import Layout from '../components/layout';
import Head from 'next/head'
import Link from 'next/link'
function HomePage() {
    return (
        <Layout>
            <Head>
                <title>Homepage</title>
            </Head>
            <div className="description">
            <h1>This is demo app, so please follow the links </h1>
            <Link href="https://hipicondemoapp.vercel.app/P/marka-x-500luk-urun-500">
               <h2>500 luk urun </h2>
            </Link>
            <Link href="https://hipicondemoapp.vercel.app/P/marka-abc-abc-kupe-200">
                <h2>ABC Kupe </h2>
            </Link>
            </div>
        </Layout>
    );
}
export default HomePage;
