import Layout from '../../components/layout';
import Head from 'next/head'
import unfetch from 'isomorphic-unfetch'
import slug from 'slug'
import Link from 'next/link'
import Card from "react-bootstrap/Card";
function ItemDetail({ singleItem, collection }) {

    return (
        <Layout>
            <Head>
                <title>Product Detail Page</title>
            </Head>


            <div className="mainItemPanel">
                <div className="mainItemImage">
                    <figure>
                        <img src={singleItem.data.imageURLs} alt={singleItem.data.name}
                            width="440" height="440" />
                    </figure>
                </div>
                <div>
                    <h1>{singleItem.data.name}</h1>
                    <h3>Brand Name: {singleItem.data.brand}</h3>
                    <h3>Price: {singleItem.data.displayPriceText}</h3>
                    <h3>Description: {singleItem.data.productStoryText}</h3>
                </div>

            </div>



            <div>
                <div className="relatedItemsPanel">
                    {collection.map((theItem, i) => {
                        return <p key={i}>
                            <Link href="/P/[slug]"
                                as={`/P/${slug(theItem.data.brand)}-${slug(theItem.data.name)}-${theItem.data.id}`} >
                                <a>

                                    <Card style={{ width: '14rem' }}>
                                        <Card.Img variant="top" src={theItem.data.imageURLs} alt={theItem.data.name} />
                                        <Card.Body>
                                            <Card.Title>{theItem.data.name}</Card.Title>
                                            <Card.Title>{theItem.data.displayPriceText}</Card.Title>

                                        </Card.Body>
                                    </Card>
                                </a>
                            </Link>
                        </p>
                    })}
                </div>
            </div>

        </Layout>
    );
}

export async function getStaticPaths() {
    const data1 = await unfetch("https://extendsclass.com/mock/rest/a58242d96d2f9c939c3a051629f2f80d/products/200")
    const item1 = await data1.json()

    const data2 = await unfetch("https://extendsclass.com/mock/rest/a58242d96d2f9c939c3a051629f2f80d/products/500")
    const item2 = await data2.json()

    let returnArray = [item1, item2];

    return {
        paths: returnArray.map(item => {
            console.log(`${slug(item.data.brand)}-${slug(item.data.name)}/${item.data.id}`)
            return { params: { slug: `${slug(item.data.brand)}-${slug(item.data.name)}-${item.data.id}` } }
            //return { params: { slug: "marka-abc-abc-kupe-200" } }
        }),
        fallback: false
    };
}

export async function getStaticProps({ params }) {
    //data fetch
    const id = params.slug.split("-").pop()
    const data = await unfetch("https://extendsclass.com/mock/rest/a58242d96d2f9c939c3a051629f2f80d/products/" + id)

    const data1 = await unfetch("https://extendsclass.com/mock/rest/a58242d96d2f9c939c3a051629f2f80d/products/200")
    const item1 = await data1.json()

    const data2 = await unfetch("https://extendsclass.com/mock/rest/a58242d96d2f9c939c3a051629f2f80d/products/500")
    const item2 = await data2.json()

    const collection = [item1, item2, item1, item2, item1];

    const singleItem = await data.json()
    return {
        props: {
            singleItem,
            collection,
        }
    }
}

export default ItemDetail;
