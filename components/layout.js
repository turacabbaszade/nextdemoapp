import Navigation from "./navigation";
import Head from 'next/head'

function Layout({ children }) {
    return (
        <div>
            <Head>
                <title>Hipicon</title>
                <meta name="viewport" content="initial-sclae=1.0, width= device-width" />
            </Head>
            <Navigation />
            <main>
                <div>{children}</div>
            </main>
        </div>
    );
}
export default Layout;
