import Modal from 'react-bootstrap/Modal'
import { useForm } from "react-hook-form"
import React, { useState } from 'react'

function Login() {
    const [show, setShow] = useState(false)

    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true)


    const { register, handleSubmit, watch, errors } = useForm();
    const onSubmit = data => {
        console.log(data);
        const requestOptions = {
            mode: "cors",
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Allow-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT',
                'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT',
                'Access-Control-Allow-Headers': '*',
                'Access-Control-Allow-Headers': 'access-control-allow-methods',
                'Access-Control-Allow-Headers': 'Access-Control-Allow-Headers, Access-Control-Allow-Credentials, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers'
            },
            body: JSON.stringify(data)
        };
        fetch('https://extendsclass.com/mock/rest/a58242d96d2f9c939c3a051629f2f80d/member/sign-in', requestOptions)
            .then(response => {
                console.log(response)
            })
    }

    return (
        <>
            <div className="headerPanel">
                <div className="headerPanelLabel">
                    <h3>HIPICON DEMO APP</h3>
                </div>
                <div className="headerPanelLabel">
                    <h6 onClick={handleShow}>Login</h6>

                    <Modal show={show} onHide={handleClose}>
                        <Modal.Header closeButton>

                        </Modal.Header>
                        <Modal.Body>
                            <form className="text-center border border-light p-5" onSubmit={handleSubmit(onSubmit)}>
                                <p className="h4 mb-4">Sign in</p>
                                <input name="email" type="email" className="form-control mb-4" placeholder="E-mail" ref={register} />
                                <input name="password" type="password" className="form-control mb-4" placeholder="Password" ref={register({ required: true })} />
                                {errors.exampleRequired && <span>This field is required</span>}
                                <input type="submit" />
                            </form>
                        </Modal.Body>
                    </Modal>
                </div>
            </div>
        </>
    )
}

export default Login
