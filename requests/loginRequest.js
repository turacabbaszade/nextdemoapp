import axios from "axios";
import { backend_address } from "../constants";


export const loginRequest = (bodyFormData) => {
    console.log(bodyFormData)
    axios({
        method: 'post',
        url: "https://extendsclass.com/mock/rest/a58242d96d2f9c939c3a051629f2f80d/member/sign-in",
        data: bodyFormData,
        mode: 'no-cors',
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
        }
    })
        .then(function (response) {
            return response;
        })
        .catch(function (response) {
            return response;
        });
};

